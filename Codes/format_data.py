import csv
import numpy

#Function to impute missing values with mean values of attribute
def ImputeMissing(X):
    means = [0.0]*len(X[0])
    for j in range(len(X[0])):
        for i in range(len(X)):
            if X[i][j]!=-1.0:
                means[j] += X[i][j]
        means[j] = float(means[j]/len(X))
    X_new = []
    for i in range(len(X)):
        curr_row = []
        for j in range(len(X[0])):
            if X[i][j]!=-1.0:
                curr_row.append(X[i][j])
            else:
                curr_row.append(means[j])
        X_new.append(curr_row)
    return X_new

#Function to normalize each value by Min-Max method
def Normalize(X):
    mins = [float("Inf")]*len(X[0])
    maxs = [0.0]*len(X[0])
    for j in range(len(X[0])):
        for i in range(len(X)):
            if X[i][j] > maxs[j]:
                maxs[j] = X[i][j]
            if X[i][j] < mins[j]:
                mins[j] = X[i][j]    
    X_new = []
    for i in range(len(X)):
        curr_row = []
        for j in range(len(X[0])):
            if(maxs[j]-mins[j])!=0:
                curr_row.append((X[i][j]-mins[j])/(maxs[j]-mins[j]))
            else:
                curr_row.append(0)
        X_new.append(curr_row)
    return X_new

fname = raw_input("Enter dataset name(entry01/ITC): ")
ds_path = "../Dataset/Original_Datasets/"+fname+".csv"

#List of output classes
classes_li = [
    'ATTACK',
    'DATABASE',
    'FTP-CO2TROL',
    'FTP-DATA',
    'FTP-PASV',
    'MAIL',
    'MULTIMEDIA',
    'P2P',
    'SERVICES',
    'WWW'
]

if fname=="ITC":
    classes_li[2]="FTP-CONTROL"
dict = {}

#Mapping class labels to integers
for i in range(len(classes_li)):
    dict[classes_li[i]] = i

dataset = []
X, Y = [], []

with open(ds_path, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i = -1
    for row in spamreader:
        if i>0:
            curr_row = row[0].split(',')
            curr_ip, curr_op = curr_row[:-1], curr_row[-1]        
            format_ip = []
            for ele in curr_ip:
                #Tranforming missing values to standard representation
                if ele=="?" or ele=="":
                    format_ip.append(-1.0)
                else:
                    format_ip.append(float(ele))
            
            X.append(format_ip)
            Y.append(dict[curr_op])
        i+=1

print("Read Dataset")
X = numpy.array(X)
Y = numpy.array(Y)

X = ImputeMissing(X)
print("Imputed Missing Values")

X = Normalize(X)
print("Normalized dataset")

for xi,yi in zip(X,Y):
    curr_row = [ele for ele in xi]
    curr_row.append(yi)
    dataset.append(curr_row)
print("Merged data")

#Writing dataset back to a new file
with open("../Dataset/"+fname+"/"+fname+"_formatted_data.csv", 'wt') as csvfile:
    spamwriter = csv.writer(csvfile)
    for row in dataset:
        spamwriter.writerow([str(ele) for ele in row])

print("Wrote Dataset")