import csv
import numpy
from random import randint, shuffle

#Class wise splitting of samples in proportion to required train/test split (0.7 of total => 0.7*class1 + 0.7*class2+..)
def SplitData(X_classwise, sr):
    no_classes = len(X_classwise)
    sr_per_class = sr                                           #float(sr/float(no_classes))
    X_train, Y_train, X_test, Y_test = [],[],[],[]
    for x_class in X_classwise:
        curr_class_tuples = x_class[1]
        no_samples_to_select = int(len(curr_class_tuples) * sr_per_class)
        prev_sel_list = []
        print("For class: {}\tSelecting {} samples".format(x_class[0], no_samples_to_select))
        for i in range(no_samples_to_select):
            new_id = randint(0, len(curr_class_tuples)-1)
            if len(prev_sel_list)!=0:
                while new_id in prev_sel_list:
                    new_id = randint(0, len(curr_class_tuples)-1)
            prev_sel_list.append(new_id)
            X_train.append(curr_class_tuples[new_id])
            Y_train.append(x_class[0])
        for i in range(len(curr_class_tuples)):
            if i not in prev_sel_list:
                X_test.append(curr_class_tuples[i])
                Y_test.append(x_class[0])
    return [X_train, Y_train, X_test, Y_test]

#Merging the classwise samples and shuffling the samples to randomize order
def MergeShuffle(X, Y):
    dataset = []
    for xi,yi in zip(X,Y):
        curr_li = [ele for ele in xi]
        curr_li.append(yi)
        dataset.append(curr_li)
    shuffle(dataset)
    return dataset

#Dataset file path
ds_name = raw_input("Enter dataset name(entry01/ITC): ")
ds_path = "../Dataset/"+ds_name+"/"+ds_name+"_formatted_data.csv"

if ds_name=='entry01':
    classes_li = [
        'ATTACK',
        'DATABASE',
        'FTP-CO2TROL',
        'FTP-DATA',
        'FTP-PASV',
        'MAIL',
        'MULTIMEDIA',
        'P2P',
        'SERVICES',
        'WWW'
    ]
elif ds_name=='ITC':
    classes_li = [
        'ATTACK',
        'DATABASE',
        'FTP-CONTROL',
        'FTP-DATA',
        'FTP-PASV',
        'MAIL',
        'MULTIMEDIA',
        'P2P',
        'SERVICES',
        'WWW'
    ]

dataset = []
X, Y = [], []

X_classwise = []
#Initializing empty samples for each class
for i in range(len(classes_li)):
    X_classwise.append([i, []])

with open(ds_path, 'rt') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
        curr_row = row[0].split(',')
        curr_ip, curr_op = curr_row[:-1], int(curr_row[-1]) 
        #Segragating samples based on class       
        X_classwise[curr_op][1].append(curr_ip)
    
s = 0

#Class distribution displayed
for cla in X_classwise:
    print("Class {} -> Number of tuples: {}\n".format(cla[0], len(cla[1])))
    s += len(cla[1])


print("Total : {}".format(s))

X = numpy.array(X)
Y = numpy.array(Y)

#Sampling ratio for overall train/test split
sr = float(input("Enter sampling ratio(0-1) for train/test split:"))

X_train, Y_train, X_test, Y_test = SplitData(X_classwise, sr)

train_dataset = MergeShuffle(X_train, Y_train)
test_dataset = MergeShuffle(X_test, Y_test)

#Writing training data to separate file
with open(ds_path[:-4]+'_train.csv', 'wt') as csvfile:
    spamwriter = csv.writer(csvfile)
    for row in train_dataset:
        spamwriter.writerow([str(ele) for ele in row])

#Writing testing data to separate file
with open(ds_path[:-4]+'_test.csv', 'wt') as csvfile:
    spamwriter = csv.writer(csvfile)
    for row in test_dataset:
        spamwriter.writerow([str(ele) for ele in row])