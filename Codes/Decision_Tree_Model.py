import csv
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
from prettytable import PrettyTable
import numpy as np
from prettytable import PrettyTable

#Dataset-CSV file path
ds_name =raw_input("Enter dataset name(entry01/ITC): ")
ds_path = "../Dataset/"+ds_name+"/"+ds_name+"_formatted_data_"


#Function to load data from CSV file
def LoadData(fn):
    X, Y = [], []
    with open(ds_path+fn, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            curr_row = row[0].split(',')
            curr_ip, curr_op = [float(ele) for ele in curr_row[:-1]], int(curr_row[-1])        
            X.append(curr_ip)
            Y.append(curr_op)
    return [np.array(X),np.array(Y)]


#Function to display result metrics
def DisplayResults(y_true, y_pred, accs):
    conf_matrix = confusion_matrix(y_true, y_pred)
    print("Confusion Matrix:")
    print(conf_matrix)
    n_classes = len(conf_matrix)
    false_pos = conf_matrix.sum(axis=0) - np.diag(conf_matrix)  
    false_neg = conf_matrix.sum(axis=1) - np.diag(conf_matrix)
    true_pos = np.diag(conf_matrix)
    true_neg = conf_matrix.sum() - (false_pos + false_neg + true_pos)
    
    col_sums = [0.0]*n_classes
    for j in range(n_classes):
        for i in range(n_classes):
            col_sums[j]+=conf_matrix[i][j]

    # calculating the precision
    precision = []

    for i in range(n_classes):
        denominator = float(true_pos[i]+false_pos[i])
        if (denominator == 0):
            precision.append(0)
        else:
            precision.append(round(float(true_pos[i]/denominator), 3))
    precision = np.array(precision)
    avg_precision = np.sum(precision)/n_classes

    # calculating the recall

    recall = []
    for i in range(n_classes):
        denominator = float(true_pos[i] + false_neg[i])
        if (denominator == 0):
            recall.append(0)
        else:
            recall.append(round(float(true_pos[i]/denominator), 3))
    recall = np.array(recall)
    avg_recall = np.sum(recall)/n_classes


    # calculating the F1 score

    f1_score = []
    for i in range(n_classes):
        denominator = float(precision[i]+recall[i])
        if (denominator == 0):
            f1_score.append(0)
        else:
            f1_score.append(round(float(2*precision[i]*recall[i]/denominator), 3))
    f1_score = np.array(f1_score)
    avg_f1_score = np.sum(f1_score)/n_classes

    # calculating the accuracy

    accs = np.array(accs)
    avg_acc = np.sum(accs)/n_classes


    results = PrettyTable(["Index", "Recall", "Precision", "F1 Score", "Accuracy"])
    for i in range(n_classes):
        results.add_row([i+1, precision[i], recall[i], f1_score[i], accs[i]])
    results.add_row(["Avg:", avg_precision, avg_recall, avg_f1_score, avg_acc])
    print (results)

X_train, Y_train = LoadData("train.csv")
print("Train data size: {}".format(len(Y_train)))

#Creating a Decision Tree Classifier instance
clf = DecisionTreeClassifier()

#Fitting the model to the dataset
clf.fit(X_train, Y_train) 

print("Finished training")

#Finding accuracy on train dataset
no_train_samples = len(Y_train)
Y_train_pred = clf.predict(X_train)
train_acc = 0
for yi,di in zip(Y_train_pred, Y_train):
    if yi==di:
        train_acc+=1
train_acc = float(train_acc/float(no_train_samples))*100.0
print("Training Accuracy: {}%".format(train_acc))    

#Finding accuracy on test dataset
X_test, Y_test = LoadData("test.csv")
print("Test data size: {}".format(len(Y_test)))
no_test_samples = len(Y_test)
Y_test_pred = clf.predict(X_test)
test_acc = 0
accs =[0]*10
di_counts=[0]*10
for yi,di in zip(Y_test_pred, Y_test):
    if yi==di:
        test_acc+=1
        accs[di]+=1
    di_counts[di]+=1
for i in range(10):
    accs[i]=round(float(accs[i]/float(di_counts[i])),3)
test_acc = float(test_acc/float(no_test_samples))*100.0
print("Testing Accuracy: {}%".format(test_acc))

#Displaying final metrics
DisplayResults(Y_test, Y_test_pred, accs)